<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Saccos extends Model
{

	public function individuals(){
		return $this->hasMany(Individuals::class , 'sacco_id', 'id');
	}

}
