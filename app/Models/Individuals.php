<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Individuals extends Model
{
/*
 * Sacco relation
 */
	public function sacco(){
		return $this->belongsTo(Saccos::class , 'sacco_id' , 'id');
	}
}
