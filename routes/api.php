<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




    //Dashboard Reports
    Route::group(['prefix' => 'dashboard', 'as' => 'dashboard'], function () {
        Route::get('/reports', ['as' => 'reports', 'uses' => 'HomeController@data']);
     
    });

Route::group(['prefix' => 'reports', 'as' => 'reports'], function () {
	Route::get('/transactions', ['as' => 'transactions', 'uses' => 'HomeController@data']);

});


Route::fallback(function(){
    return response()->json([
         'message' => 'Page Not Found. If error persists, contact jacjimus@gmail.com'], 404);

});

