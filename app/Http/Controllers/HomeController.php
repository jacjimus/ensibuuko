<?php

namespace App\Http\Controllers;

use App\Http\Resources\DashboardResource;
use App\Models\Saccos;
use App\Models\Transactions;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
//	    $data = Transactions::select(DB::raw("saccos.country, count(saccos.id) as saccos,transactions.type,sum(amount) as total"))
//		    ->join('individuals' , 'individuals.id', '=','transactions.individual_id' )
//		    ->join('saccos' ,'saccos.id','=','individuals.sacco_id')
//		    ->groupBy("saccos.country")
//		    ->groupBy("transactions.type")
//		    ->get();
//	    $results = [];
//	    $unique_countries = [];
//	    foreach ($data AS $d):
//		    if(!in_array($d->country , $unique_countries)):
//			    $set['country'] = $d->country;
//			    $set['saccos'] = $d->saccos;
//
//			    $set['deposited'] =  $this->getCountryData($data , $d->country, 'deposit');
//			    $set['withdrawn'] = $this->getCountryData($data , $d->country, 'withdrawal');
//			    $set['netgain'] = ($set['deposited'] - $set['withdrawn']);
//			    array_push($results , $set);
//		    endif;
//
//
//
//		    array_push($unique_countries , $d->country);
//
//
//	    endforeach;

	     return view('home');
    }

	public function data()
	{
		$data = Transactions::select(DB::raw("saccos.country, count(saccos.id) as saccos,transactions.type,sum(amount) as total"))
			->join('individuals' , 'individuals.id', '=','transactions.individual_id' )
			->join('saccos' ,'saccos.id','=','individuals.sacco_id')
			->groupBy("saccos.country")
			->groupBy("transactions.type")
			->get();
		$results = [];
		$unique_countries = [];
		foreach ($data AS $d):
			if(!in_array($d->country , $unique_countries)):
				$set['country'] = $d->country;
				$set['saccos'] = Saccos::where('country' , $d->country)->count();
				$set['deposited'] =  $this->getCountryData($data , $d->country, 'deposit');
				$set['withdrawn'] = $this->getCountryData($data , $d->country, 'withdrawal');
				$set['netgain'] = ($set['deposited'] - $set['withdrawn']);
				array_push($results , $set);
			endif;



			array_push($unique_countries , $d->country);

		endforeach;
		return new DashboardResource($results);
	}

	protected function getCountryData($data , $key, $type){
    	$unique_countries = [];
    	foreach ($data AS $d){
    		 if ($key == $d->country && !in_array($key , $unique_countries) && $d->type == $type) {
			     array_push($unique_countries, $key);
			     return $d->total;
		     }
		}


	}



}
